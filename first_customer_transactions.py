import pandas as pd
from sqlalchemy.engine import url as sa_url
from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import text
import logging
from datetime import datetime
import pathlib
import os

class Redshift:
    """
    Class to manage the connection to redshift

    Parameters
    ----------

    host:str

    port:int

    dbname:str

    user:str

    password:str

    Properties
    ----------

    connection_url:str

    engine:sqlalchemy.Engine

    """

    def __init__(self, host, port, dbname, user, password, echo=False):

        self.logger = logging.getLogger(self.__class__.__name__)

        self.connection_url = sa_url.URL(
            drivername='postgresql+psycopg2',
            username=user,
            password=password,
            host=host,
            port=port,
            database=dbname
        )

        self.logger.debug(f'Connection URL: {self.connection_url}')

        try:
            self.engine = create_engine(self.connection_url, echo=echo)
        except Exception as e:
            self.logger.error("Error Connecting to redshift")
            self.logger.exception(e)
            raise e

    def query(self, query, return_df=False, chunksize=None):
        """
        Run a query in the connected database

        Parameters
        ----------
        query:str
        The SQL code to run in the database

        return_df:bool
        Do you want to return the results as a dataframe?

        Returns
        -------
        pd.DataFrame
        The results of your query
        """
        _s = datetime.now()

        # if you want to return the method to return a dataframe then we use a different method
        if return_df:
            output = self._df_query(query, chunksize)

        # If they do not want the results we can just execute the query on the redshift side
        else:
            self._raw_query(query)
            output = None

        _e = datetime.now()
        self.logger.info(f"Query took {str(_e - _s)}")

        return output

    def _df_query(self, query: str, chunksize: int = None) -> pd.DataFrame:
        """Run a query against redshift and return a dataframe

        Args:
            query (str): The query
            chunksize (int): To chunk the output enter a value. Defaults to None.

        Raises:
            e: Raises an error if the query fails

        Returns:
            pd.DataFrame: The output Dataframe
        """
        try:
            output = pd.read_sql(sql=query, con=self.engine,
                                 chunksize=chunksize)
            self.logger.info(f"Output df shape: {output.shape}")
        except Exception as e:
            self.logger.error("Error running query to get dataframe")
            self.logger.exception(e)
            raise e

        return output

    def _raw_query(self, query: str) -> None:
        """Execute a query in redshift and don't extract any results

        Args:
            query (str): The query

        Raises:
            e: Any error raised during execution
        """
        statement = text(query)

        try:
            with self.engine.connect() as con:
                con.execute(statement)
        except Exception as e:
            self.logger.error("Error running query")
            self.logger.exception(e)
            raise e

    def close(self) -> None:
        """
        Close the connection to redshift
        """
        self.engine.dispose()

    def table_exists(self, table_name: str, schema: str = None) -> bool:
        """
        Check if a table exists in the data warehouse
        """
        return self.engine.dialect.has_table(self.engine, table_name, schema)

    def upload_dataframe(
        self,
        df: pd.DataFrame,
        table_name: str,
        schema: str = None,
        if_exists: str = 'append',
        index: str = False,
        index_label: str = None,
        chunksize: int = 10000,
        dtype: dict = None,
        method: str = 'multi'
    ) -> None:
        """
        Upload a dataframe to the redshift data warehouse. 

        Used the `pd.DataFrame.to_sql` method but changed some defaults

        Parameters
        ----------
        Look at the parameters for `pd.DataFrame.to_sql`
        """

        self.logger.info(f"Uploading dataframe of shape: {df.shape}")
        _s = datetime.now()

        df.to_sql(
            table_name,
            con=self.engine,
            schema=schema,
            if_exists=if_exists,
            index=index,
            index_label=index_label,
            chunksize=chunksize,
            dtype=dtype,
            method=method
        )

        _e = datetime.now()
        self.logger.info(f"upload_table took {str(_e - _s)}s")

    def drop_table(self, table_name: str, schema: str = None) -> None:
        """
        Drop a table in redshift
        """

        _s = datetime.now()

        self.logger.info(f"Drop Table {schema}.{table_name}")
        self.query(f'drop table if exists {schema}.{table_name};')

        _e = datetime.now()
        self.logger.info(f"drop_table took {str(_e - _s)}s")

    def get_count(self, table_name: str, schema: str,  **kwargs) -> int:
        """
        Get the number of rows in a given table given conditions
        """

        where_conditions = '\n'.join(
            [f"and {k} = {v}" for k, v in kwargs.items()])

        sql = f"""
        select count(*) from {schema}.{table_name}
        where 1=1
        {where_conditions}
        """

        count = self.query(sql, return_df=True)

        return count.values[0][0]

    def delete_from(self, table_name: str, schema: str, **kwargs) -> None:
        """
        Delete from a table given conditions
        """

        where_conditions = '\n'.join(
            [f"and {k} = {v}" for k, v in kwargs.items()])

        sql = f"""
        delete from {schema}.{table_name}
        where 1=1
        {where_conditions}
        """

        self.query(sql)

# query to create the table
sql_initialise_table = """
create table {full_table_name} as
select
    mobile_customer_id
    ,min(transaction_date_sk) as first_transaction_date_sk
    ,max(transaction_date_sk) as last_transaction_date_sk
    ,count(distinct transaction_date_sk || '_' || transaction_id || '_' || store_sk) as num_transactions
    ,getdate() as adt_creation
    ,getdate() as adt_modified
from dw_pre.f_stld_detail

where mobile_customer_id not in ('', '0')

group by 1
"""

# query to update the table
sql_update_table = """
create table #new_data as
with new_transactions as (
select
    mobile_customer_id
    ,min(transaction_date_sk) as first_transaction_date_sk
    ,max(transaction_date_sk) as last_transaction_date_sk
    ,count(distinct transaction_date_sk || '_' || transaction_id || '_' || store_sk) as num_transactions
    ,count(distinct case when transaction_date_sk = {max_date_sk} then transaction_date_sk || '_' || transaction_id || '_' || store_sk end) as max_date_transactions -- watch out for double counting
    ,getdate() as adt_creation
    ,getdate() as adt_modified
from dw_pre.f_stld_detail

where mobile_customer_id not in ('', '0')
and transaction_date_sk >= {max_date_sk} -- catch late additions

group by 1
)

select
    coalesce(a.mobile_customer_id, b.mobile_customer_id) as mobile_customer_id
    ,coalesce(a.first_transaction_date_sk, b.first_transaction_date_sk) as first_transaction_date_sk
    ,coalesce(b.last_transaction_date_sk, a.last_transaction_date_sk) as last_transaction_date_sk
    ,coalesce(a.num_transactions, 0) + coalesce(b.num_transactions, 0) - coalesce(max_date_transactions, 0) as num_transactions
    ,coalesce(a.adt_creation, b.adt_creation) as adt_creation
    ,coalesce(b.adt_modified, a.adt_modified) as adt_modified
from {full_table_name} a

full outer join new_transactions b
on a.mobile_customer_id = b.mobile_customer_id;

-- remove all data from the target table
truncate {full_table_name};

-- insert the new data into the target table
insert into {full_table_name}
select * from #new_data;

-- drop the temporary table
drop table #new_data;
"""


class CustomerFirstTransaction:

    def __init__(self, rs: Redshift, table_name='customer_first_transaction', schema='annalect') -> None:

        self.rs = rs
        self.table_name = table_name
        self.schema = schema

        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info(f"Results to be stored in {self.full_table_name}")

    @property
    def full_table_name(self):
        return f"{self.schema}.{self.table_name}"

    def __initialise_table(self):

        self.logger.info("Initialising the table")

        query = sql_initialise_table.format(
            full_table_name=self.full_table_name)

        self.logger.debug(query)

        self.rs.query(query)

    def __update_table(self):

        self.logger.info("Updating table")

        max_date_sk = self.rs.query(
            f"select max(last_transaction_date_sk) from {self.full_table_name}",
            return_df=True
        ).values[0][0]

        self.logger.info(f"Last update: {max_date_sk}")

        query = sql_update_table.format(
            full_table_name=self.full_table_name,
            max_date_sk=max_date_sk
        )

        self.logger.debug(query)

        self.rs.query(query)

    def run(self):

        self.logger.info("Running...")

        if self.rs.table_exists(self.table_name, self.schema):

            # if the table exists we just want to update it
            try:
                self.__update_table()
            except Exception as e:
                self.logger.error("Error updating the table")
                self.logger.exception(e)
        else:
            # otherwise we want to create it
            try:
                self.__initialise_table()
            except Exception as e:
                self.logger.error("Error initialising the table")
                self.logger.exception(e)

        self.logger.info("Complete")


credentials = {
    'host': 'us-east-cprod-mdap-au-redshift1.ciuu2sg591tg.us-east-1.redshift.amazonaws.com',
    'dbname': 'dev',
    'port': '5439',
    'user': 'tim_renton',
    'password': '8zZa@h13pL'
}

if __name__ == "__main__":

    print("Running")

    logging.basicConfig(
        filename=os.path.join(
            pathlib.Path(__file__).parent.absolute(),
            'first_transaction.log'
        ),
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.DEBUG
    )

    try:
        # create the redshift engine
        rs = Redshift(**credentials)

        # create the processing class
        cft = CustomerFirstTransaction(
            rs,
            table_name='customer_first_transaction',
            schema='annalect'
        )

        # run the process
        cft.run()
    except Exception as e:
        logging.error("Error Running Script")
        logging.exception(e)
        raise e
